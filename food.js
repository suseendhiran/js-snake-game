import { onSnake, expandSnake } from "./snake.js";
import { randomGridposition } from "./grid.js";

export const SNAKE_SPEED = 6;
const EXPANSION_RATE = 5;
let food = getRandomposition();
let gameStart = true;

export function update() {
  if (onSnake(food)) {
    expandSnake(EXPANSION_RATE);
    food = getRandomposition();
  }
}
export function draw(gameboard) {
  const foodelement = document.createElement("div");

  foodelement.style.gridRowStart = food.y;
  foodelement.style.gridColumnStart = food.x;
  foodelement.classList.add("food");
  gameboard.appendChild(foodelement);
}

function getRandomposition() {
  let newFoodposition;
  while (newFoodposition == null || onSnake(newFoodposition)) {
    newFoodposition = randomGridposition();
  }

  return newFoodposition;
}
