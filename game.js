import { SNAKE_SPEED } from "./snake.js";
import {
  update as updatesnake,
  draw as drawsnake,
  getSnakehead,
  snakeIntersection,
} from "./snake.js";
import { update as updatefood, draw as drawfood } from "./food.js";
import { outsideGrid } from "./grid.js";
let lastRendertime = 0;
let gameOver = false;
let gameStart = true;
const gameBoard = document.getElementById("game-board");
if (
  confirm(
    `Welcome to Snake Xenzia game! Use Arrow Keys to Control the Snake. Eat food(yellow) to Grow the Snake. if Snake bites itself or the borders, Game is Over!!Click OK to Play`
  )
) {
  function main(currentTime) {
    if (gameOver) {
      if (confirm("You lose, Press ok to continue")) {
        window.location = "/";
      }
      return;
    }
    window.requestAnimationFrame(main);

    const secondsSincelastrender = (currentTime - lastRendertime) / 1000;
    //   console.log("before", secondsSincelastrender);
    if (secondsSincelastrender < 1 / SNAKE_SPEED) return;
    lastRendertime = currentTime;
    console.log("gap", secondsSincelastrender);
    update();
    draw();
  }
  window.requestAnimationFrame(main);

  async function update() {
    updatesnake();
    updatefood();
    checkEnd();
    // document.getElementById("game-board").style.backgroundColor = "white";
  }

  function draw() {
    gameBoard.innerHTML = "";
    drawsnake(gameBoard);
    drawfood(gameBoard);
  }

  function checkEnd() {
    gameOver = outsideGrid(getSnakehead()) || snakeIntersection();
  }
}
