import { getInputdirection } from "./input.js";

export const SNAKE_SPEED = 6;
const snakeBody = [{ x: 11, y: 11 }];

let newSegments = 0;

export function update() {
  console.log("snake update");
  addSegment();
  const inputdirection = getInputdirection();
  for (let i = snakeBody.length - 2; i >= 0; i--) {
    snakeBody[i + 1] = { ...snakeBody[i] };
    console.log(i);
    console.log(snakeBody[i + 1]);
  }
  console.log({ ...snakeBody });
  console.log("first body", { ...snakeBody[0] });
  snakeBody[0].x += inputdirection.x;
  snakeBody[0].y += inputdirection.y;
}
export function draw(gameboard) {
  console.log("snake draw");
  snakeBody.forEach((segment) => {
    const snakeElement = document.createElement("div");
    snakeElement.style.gridRowStart = segment.y;
    snakeElement.style.gridColumnStart = segment.x;
    snakeElement.classList.add("snake");
    gameboard.appendChild(snakeElement);
  });
}

export function expandSnake(amount) {
  newSegments += amount;
}

export function onSnake(position, { ignoreHead = false } = {}) {
  return snakeBody.some((segment, index) => {
    if (ignoreHead && index === 0) return false;
    return equalPositions(segment, position);
  });
}
function equalPositions(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}
export function getSnakehead() {
  return snakeBody[0];
}
export function snakeIntersection() {
  return onSnake(snakeBody[0], { ignoreHead: true });
}

function addSegment() {
  for (let i = 0; i < newSegments; i++) {
    snakeBody.push({ ...snakeBody[snakeBody.length - 1] });
  }
  newSegments = 0;
}
